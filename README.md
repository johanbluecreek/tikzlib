# My Tikz library

A form of library where I collect all my tikz pictures so I do not have to go hunting for them trough out my presentations and/or publications.

See pdf for the pictures, the tex for the code.
